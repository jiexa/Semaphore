package com.malyshev;

/**
 * Created by jiexa on 10.02.17.
 */
public class Prostator implements Runnable {

    private int[] array;
    private Consumer[] cons;

    public Prostator(int[] array, Consumer[] cons) {
        this.array = array;
        this.cons = cons;
    }

    @Override
    public void run() {
        int number;
        for (int i : array) {
            number = i;
            for (Consumer con : cons) {
                con.printSum(0, 0, number);
            }
        }
    }

}
