package com.malyshev;

/**
 * Created by jiexa on 10.02.17.
 */
public class Cubator implements Runnable {

    private final int[] array;
    private final Consumer[] cons;

    public Cubator(int[] array, Consumer[] cons) {
        this.array = array;
        this.cons = cons;

    }

    @Override
    public void run() {
        for (int i : array) {
            int cube = i * i * i;
            for (Consumer consumer : cons) {
                consumer.printSum(cube, 0, 0);
            }
        }

    }

}
