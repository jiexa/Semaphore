package com.malyshev;

public class Main {

    public static void main(String[] args) {

        int[] array = {1, 2, 3};

        Consumer[] cons = {new Consumer()};
        Cubator cubator = new Cubator(array, cons);
        Quadrator quadrator = new Quadrator(array, cons);
        Prostator prostator = new Prostator(array, cons);

        Thread cubatorThr = new Thread(cubator);
        Thread quadratorThr = new Thread(quadrator);
        Thread prostatorThr = new Thread(prostator);

        cubatorThr.start();
        quadratorThr.start();
        prostatorThr.start();

    }
}
