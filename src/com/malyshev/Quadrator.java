package com.malyshev;

/**
 * Created by jiexa on 10.02.17.
 */
public class Quadrator implements Runnable {

    private int[] array;
    private Consumer[] cons;

    public Quadrator(int[] array, Consumer[] cons) {
        this.array = array;
        this.cons = cons;
    }

    @Override
    public void run() {
        int square;
        for (int i : array) {
            square = i * i;
            for (Consumer consumer : cons) {
                consumer.printSum(0, square, 0);
            }
        }
    }
}
