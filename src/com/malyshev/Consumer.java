package com.malyshev;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by jiexa on 10.02.17.
 */
public class Consumer {

    private AtomicInteger sum = new AtomicInteger(0);
    private AtomicBoolean lock = new AtomicBoolean(false);

    public void printSum (int cube, int square, int number) {
        if (cube != 0) {
            lockAndAdd(lock, cube);
        }
        if (square != 0) {
            lockAndAdd(lock, square);
        }
        if (number != 0) {
            lockAndAdd(lock, number);
        }
    }

    private void lockAndAdd(AtomicBoolean lock, int value) {

        while (!lock.compareAndSet(false, true)) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int curValue = sum.addAndGet(value);
        System.out.println("Value = " + curValue);
        lock.getAndSet(false);

    }
}
